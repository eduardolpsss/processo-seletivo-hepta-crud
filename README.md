# processo-seletivo-hepta-crud
<p>
    Sistema desenvolvido especificamente para o processo seletivo de desenvolvedor Java Jr. da Hepta tecnologia, utilizei a linguagem Java em seu back-end e VUE.js em seu front-end.
</p>

<div align="center">
    <img src="https://user-images.githubusercontent.com/64506852/192931450-327fd000-10e3-4b66-9a97-4c2dabd15304.png"/>
</div>
